# trywake-tts

### 前置条件
```python
pip install edge-tts
pip install fastapi
pip install uvicorn
```

#### 介绍
基于微软开源edge-tts在线TTS工具，文本生成语音文件后可下载，也可批量打包下载

### 技术栈
python3.11 + vue3 + fastapi + uvicorn + edge-tts

### 目录说明
tts-ui为前端UI项目，主要注意App.vue的baseUrl路径配置以及vite.config.ts里的base前端资源路径配置，打包后部署到templates目录下即可；
static目录下存储的是转换后的语音文件

#### 运行
```shell
uvicorn main:app --reload
```

![主页演示](images/image.png)

##### 如果这个小工具帮上您的忙了，我不介意您请我喝杯茶的 ^_^！

<img alt="小小鼓励" height="100" src="images/pay.jpg" width="100"/>